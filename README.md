# PCG Dungeon Generation

This project focused on the methoods of Procedurally generating a level. After reviewing several algorithms for generation, I settled on the Subdivision method. In this methood, rules determine how subdivisions can be made and then are applied to the specified world space to generate initial layout.  
Rooms were then given logic for what space requirements are needed for different occurences and populated accordingly.

# Project Specifications

This project was created in Unreal Engine 5.0 in two weeks.

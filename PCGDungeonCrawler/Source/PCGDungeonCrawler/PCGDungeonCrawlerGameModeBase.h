// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PCGDungeonCrawlerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API APCGDungeonCrawlerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/PuzzleRoomManager.h"

#include "PCG/DungeonGenerationSubdivisionNode.h"
#include "PCG/ClosableDoorObject.h"
#include "PCG/TriggerBoxGeneratable.h"

void APuzzleRoomManager::SetupRoom(UDungeonGenerationSubdivisionNode* puzzleRoom)
{
	if (puzzleRoom == nullptr)
	{
		return;
	}
	_puzzleRoom = puzzleRoom;
	_puzzleRoom->OnRoomSpawnCompleted().AddUObject(this, &APuzzleRoomManager::PuzzleRoomSpawned);
}

void APuzzleRoomManager::PuzzleRoomSpawned()
{
	for (auto& tile : _puzzleRoom->SpawnedTiles)
	{
		auto doorTile = Cast<AClosableDoorObject>(tile);
		if (doorTile)
		{
			_doors.Add(doorTile);
		}
		else
		{
			auto triggerTile = Cast<ATriggerBoxGeneratable>(tile);
			if (triggerTile)
			{
				triggerTile->OnTileTriggered().AddUObject(this, &APuzzleRoomManager::BeginPuzzle);
			}
		}
	}
	SetupPuzzle();
}

void APuzzleRoomManager::SetupPuzzle()
{}

void APuzzleRoomManager::BeginPuzzle()
{
	if (puzzleBegun)
	{
		return;
	}
	puzzleBegun = true;
	for (auto& door : _doors)
	{
		door->CloseDoor();
	}
}

void APuzzleRoomManager::PuzzleSolved()
{
	for (auto& door : _doors)
	{
		door->OpenDoor();
	}
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PCG/GeneratableObject.h"
#include "PlayerStartGeneratable.generated.h"

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API APlayerStartGeneratable : public AGeneratableObject
{
	GENERATED_BODY()
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PuzzleRoomManager.generated.h"

class UDungeonGenerationSubdivisionNode;
class AClosableDoorObject;

UCLASS()
class PCGDUNGEONCRAWLER_API APuzzleRoomManager : public AActor
{
	GENERATED_BODY()

public:
	virtual void SetupRoom(UDungeonGenerationSubdivisionNode* puzzleRoom);

protected:

	UPROPERTY()
	UDungeonGenerationSubdivisionNode* _puzzleRoom;

	bool puzzleBegun = false;


	void BeginPuzzle();
	void PuzzleSolved();

	TArray<AClosableDoorObject*> _doors;

	UFUNCTION()
	virtual void SetupPuzzle();

private:
	void PuzzleRoomSpawned();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/DungeonGenerationSubdivisionNode.h"

UDungeonGenerationSubdivisionNode* UDungeonGenerationSubdivisionNode::CreateTree(
		FDungeonGenerationInfo dungeonGenerationInfo
		, int x
		, int y)
{
	auto node = NewObject<UDungeonGenerationSubdivisionNode>(UDungeonGenerationSubdivisionNode::StaticClass());
	node->InitializeNode(dungeonGenerationInfo, x, y,
		dungeonGenerationInfo.SpaceWidth, dungeonGenerationInfo.SpaceHeight);
	return node;
}

const TArray<UDungeonGenerationSubdivisionNode*>& UDungeonGenerationSubdivisionNode::GetChildren() const
{
	return _children;
}

bool UDungeonGenerationSubdivisionNode::IsLeafNode() const
{
	return _children.Num() == 0;
}

// Setup this nodes variables and then recursively construct its children as necissary
int UDungeonGenerationSubdivisionNode::InitializeNode(
		FDungeonGenerationInfo dungeonGenerationInfo
		, int x
		, int y
		, int width
	, int length)
{
	// Set variables
	_x = x;
	_y = y;
	_width = width;
	_length = length;

	// If can be split on x or y axis, generate a random number.
	int roomProbabilities[2] = { -1, -1 };
	if (_width >= dungeonGenerationInfo.MinRoomWidth * 2 && _width > dungeonGenerationInfo.MaxRoomWidth)
	{
		roomProbabilities[0] = FMath::RandRange(0, 100);
	}
	if (_length >= dungeonGenerationInfo.MinRoomLength * 2 && _length > dungeonGenerationInfo.MaxRoomLength)
	{
		roomProbabilities[1] = FMath::RandRange(0, 100);
	}

	// If Neither width nor height can be a room then we are at a leaf node
	if (roomProbabilities[0] == -1 && roomProbabilities[1] == -1)
	{
		defaultTile = dungeonGenerationInfo.floorType;
		SetupWalls(dungeonGenerationInfo.wallType);
		return 1;
	}

	// adjust the axis variables as needed based on the randomly generated numbers from earlier.
	bool splitOnWidth = roomProbabilities[0] > roomProbabilities[1];
	int basePosition = splitOnWidth ? x : y;
	int* splitPosition = splitOnWidth ? &x : &y;
	int* splitDimension = splitOnWidth ? &width : &length;
	int minSplit = splitOnWidth ? dungeonGenerationInfo.MinRoomWidth : dungeonGenerationInfo.MinRoomLength;
	
	// Set the position of the split along the chosen axis
	*splitPosition = FMath::RandRange(*splitPosition + minSplit, *splitPosition + *splitDimension - minSplit);
	*splitDimension = *splitPosition - basePosition;

	// Create first child and initialize it.
	auto child1 = NewObject<UDungeonGenerationSubdivisionNode>(UDungeonGenerationSubdivisionNode::StaticClass());
	int numNodes = child1->InitializeNode(dungeonGenerationInfo, _x, _y, width, length);

	// Create second child and initialize it.
	auto child2 = NewObject<UDungeonGenerationSubdivisionNode>(UDungeonGenerationSubdivisionNode::StaticClass());
	numNodes += child2->InitializeNode(dungeonGenerationInfo, x, y, _width - (x - _x), _length - (y - _y));

	_children.Add(child1);
	_children.Add(child2);

	child1->ConnectToNode(child2, dungeonGenerationInfo.doorType);

	// return the total nodes created by children.
	return numNodes;
}

bool UDungeonGenerationSubdivisionNode::ConnectToNode(UDungeonGenerationSubdivisionNode* otherNode, TSubclassOf<AGeneratableObject> doorType)
{
	while(!otherNode->IsLeafNode())
	{
		otherNode = otherNode->_children[0];
	}
	if (!IsLeafNode())
	{
		if (!_children[0]->ConnectToNode(otherNode, doorType))
		{
			return (_children[1]->ConnectToNode(otherNode, doorType));
		}
		return true;
	}
	else
	{
		bool bordersOnX = _x == otherNode->_x;
		if (_y + _length != otherNode->_y && _x + _width != otherNode->_x)
		{
			return false;
		}
		// Set Door Tile for connected rooms
		int x = bordersOnX ? (_width <= otherNode->_width ? _width : otherNode->_width) : _width;
		int y = bordersOnX ? _length : (_length <= otherNode->_length ? _length : otherNode->_length);
		int& borderingDimension = bordersOnX ? x : y;
		int& otherPos = bordersOnX ? y : x;
		borderingDimension /= 2;
		otherPos--;
		_tileMap[y * _width + x] = doorType;
		otherPos = 0;
		otherNode->_tileMap[y * otherNode->_width + x] = doorType;
		_connectedRooms.Add(otherNode);
		otherNode->_connectedRooms.Add(this);
		return true;
	}
}


int UDungeonGenerationSubdivisionNode::X() const
{
	return _x;
}

int UDungeonGenerationSubdivisionNode::Y() const
{
	return _y;
}

int UDungeonGenerationSubdivisionNode::Width() const
{
	return _width;
}

int UDungeonGenerationSubdivisionNode::Length() const
{
	return _length;
}

void UDungeonGenerationSubdivisionNode::SetTile(int x, int y, TSubclassOf<AGeneratableObject> tileType)
{
	int idx = y * _width + x;
	if (idx < _tileMap.Num())
	{
		_tileMap[idx] = tileType;
	}
}

void UDungeonGenerationSubdivisionNode::SetupWalls(TSubclassOf<AGeneratableObject> wallType)
{
	_tileMap.SetNum(_width * _length);
	for (int wallX = 0; wallX < _width; wallX++)
	{
		if (wallX == 0 || wallX == _width - 1)
		{
			for (int wallY = 0; wallY < _length; wallY++)
			{
				_tileMap[wallY * _width + wallX] = wallType;
			}
		}
		else
		{
			_tileMap[wallX] = wallType;
			_tileMap[_tileMap.Num() - 1 - wallX] = wallType;
		}
	}
}

void UDungeonGenerationSubdivisionNode::SpawnRoom(UWorld * world, float tileScale)
{
	if (world == nullptr)
	{
		return;
	}
	float startX = _x * tileScale;
	float startY = _y * tileScale;
	for (int x = 0; x < _width; x++)
	{
		for (int y = 0; y < _length; y++)
		{
			auto tileType = _tileMap[y * _width + x];
			tileType = tileType ? tileType : defaultTile;
			auto tile = world->SpawnActor<AGeneratableObject>(tileType);
			if (tile)
			{
				FVector location(startX, startY, 0);
				location.X += tileScale * x;
				location.Y += tileScale * y;
				tile->SetActorLocation(location);
				if (y == 0 || y == _length - 1)
				{
					tile->AddActorWorldRotation(FRotator(0, 90, 0));
				}
				SpawnedTiles.Add(tile);
			}
		}
	}
	_roomSpawnEvent.Broadcast();
}

const TArray<UDungeonGenerationSubdivisionNode*>& UDungeonGenerationSubdivisionNode::GetConnections() const
{
	return _connectedRooms;
}

TSubclassOf<AGeneratableObject> UDungeonGenerationSubdivisionNode::GetTile(int x, int y) const
{
	return _tileMap[y * _width + x];
}

UDungeonGenerationSubdivisionNode::FRoomSpawnEvent& UDungeonGenerationSubdivisionNode::OnRoomSpawnCompleted()
{
	return _roomSpawnEvent;
}
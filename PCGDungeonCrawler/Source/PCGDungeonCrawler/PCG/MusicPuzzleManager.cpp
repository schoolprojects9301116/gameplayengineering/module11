// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/MusicPuzzleManager.h"
#include "PCG/DungeonGenerationSubdivisionNode.h"
#include "PCG/TriggerBoxGeneratable.h"

#include "Kismet/GameplayStatics.h"

AMusicPuzzleManager::AMusicPuzzleManager()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AMusicPuzzleManager::SetupPuzzle()
{
	int toneNumber = 0;
	for (auto& tile : _puzzleRoom->SpawnedTiles)
	{
		auto triggerTile = Cast<ATriggerBoxGeneratable>(tile);
		if (triggerTile)
		{
			if (tile->IsA(sampleBlock))
			{
				triggerTile->OnTileTriggered().AddUObject(this, &AMusicPuzzleManager::PuzzleSampleActivated);
			}
			else if (tile->IsA(puzzleBlock))
			{
				triggerTile->OnTileTriggered().AddUObject(this, &AMusicPuzzleManager::PuzzleBlockActivated, toneNumber++);
			}
		}
	}

	int numTones = FMath::RandRange(minSounds, maxSounds);
	for (int i = 0; i < numTones; i++)
	{
		puzzleSolution.Add(FMath::RandRange(0, 2));
	}
}

void AMusicPuzzleManager::PuzzleBlockActivated(int soundIdx)
{
	if (puzzleSampleIsPlaying)
	{
		puzzleSampleIsPlaying = false;
		GetWorldTimerManager().ClearTimer(sampleTimerHandle);
		Reset();
	}
	if (soundIdx == puzzleSolution[solutionIndex])
	{
		UGameplayStatics::PlaySound2D(GetWorld(), PuzzleTones[soundIdx]);
		solutionIndex++;
		puzzleTimer = 0.0f;
		puzzleIsReset = false;
		if (solutionIndex >= puzzleSolution.Num())
		{
			PuzzleSolved();
			Destroy();
		}
	}
	else if (puzzleIsReset)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), PuzzleTones[soundIdx]);
	}
	else
	{
		ResetPuzzle();
	}
}

void AMusicPuzzleManager::PuzzleSampleActivated()
{
	if (puzzleSampleIsPlaying)
	{
		GetWorldTimerManager().ClearTimer(sampleTimerHandle);
	}
	ResetPuzzle();
	puzzleSampleIsPlaying = true;
	GetWorldTimerManager().SetTimer(sampleTimerHandle, this, &AMusicPuzzleManager::PlaySample, sampleRepeat, true, sampleDelay);
}

void AMusicPuzzleManager::PlaySample()
{
	UGameplayStatics::PlaySound2D(GetWorld(), PuzzleTones[puzzleSolution[sampleIndex]]);
	sampleIndex++;
	if (sampleIndex >= puzzleSolution.Num())
	{
		sampleIndex = 0;
		puzzleSampleIsPlaying = false;
		GetWorldTimerManager().ClearTimer(sampleTimerHandle);
	}
}

void AMusicPuzzleManager::ResetPuzzle()
{
	UGameplayStatics::PlaySound2D(GetWorld(), FailureTone);
	puzzleTimer = 0.0f;
	puzzleIsReset = true;
	solutionIndex = 0;
	sampleIndex = 0;
}

void AMusicPuzzleManager::SetBlockTypes(TSubclassOf<ATriggerBoxGeneratable> sampleType, TSubclassOf<ATriggerBoxGeneratable> puzzleType)
{
	sampleBlock = sampleType;
	puzzleBlock = puzzleType;
}

void AMusicPuzzleManager::Tick(float DeltaTime)
{
	if (!puzzleIsReset)
	{
		puzzleTimer += DeltaTime;
		if (puzzleTimer >= puzzleResetTime)
		{
			ResetPuzzle();
		}
	}
}

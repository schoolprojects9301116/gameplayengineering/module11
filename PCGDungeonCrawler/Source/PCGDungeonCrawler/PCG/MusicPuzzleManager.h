// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Sound/SoundCue.h"

#include "CoreMinimal.h"
#include "PCG/PuzzleRoomManager.h"
#include "MusicPuzzleManager.generated.h"

class ATriggerBoxGeneratable;

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API AMusicPuzzleManager : public APuzzleRoomManager
{
	GENERATED_BODY()

public:
	AMusicPuzzleManager();
	virtual void Tick(float DeltaTime) override;

	void SetBlockTypes(TSubclassOf<ATriggerBoxGeneratable> sampleType, TSubclassOf<ATriggerBoxGeneratable> puzzleType);

protected:
	virtual void SetupPuzzle() override;

	UFUNCTION()
	void PuzzleBlockActivated(int soundIdx);

	UFUNCTION()
	void PuzzleSampleActivated();

	UFUNCTION()
	void ResetPuzzle();

	UFUNCTION()
	void PlaySample();

	UPROPERTY(EditAnywhere)
	float puzzleResetTime = 3.f;

	UPROPERTY(EditAnywhere)
	TArray<USoundCue*> PuzzleTones;

	UPROPERTY(EditAnywhere)
	USoundCue* FailureTone;

	UPROPERTY(EditAnywhere)
	int minSounds = 3;

	UPROPERTY(EditAnywhere)
	int maxSounds = 8;

	UPROPERTY()
	TArray<int> puzzleSolution;

	UPROPERTY()
	TSubclassOf<ATriggerBoxGeneratable> sampleBlock;

	UPROPERTY()
	TSubclassOf<ATriggerBoxGeneratable> puzzleBlock;

	UPROPERTY()
	FTimerHandle sampleTimerHandle;

	UPROPERTY(EditAnywhere)
	float sampleRepeat = 1.f;

	UPROPERTY(EditAnywhere)
	float sampleDelay = 1.f;

	float puzzleTimer = 0.0f;
	int solutionIndex = 0;
	int sampleIndex = 0;
	bool puzzleIsReset = true;
	bool puzzleSampleIsPlaying = false;
};

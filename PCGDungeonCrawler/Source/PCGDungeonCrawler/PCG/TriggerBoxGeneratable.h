// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "PCG/GeneratableObject.h"
#include "TriggerBoxGeneratable.generated.h"


/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API ATriggerBoxGeneratable : public AGeneratableObject
{
	GENERATED_BODY()
	
public:
	ATriggerBoxGeneratable();

	DECLARE_EVENT(ATriggerBoxGeneratable, FTriggerTileEvent)
	FTriggerTileEvent& OnTileTriggered();

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
			, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
		, const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere)
	UBoxComponent* _triggerBox;

private:
	FTriggerTileEvent tileTriggeredEvent;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PCG/GeneratableObject.h"
#include "EndingGeneratable.generated.h"

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API AEndingGeneratable : public AGeneratableObject
{
	GENERATED_BODY()
	
};

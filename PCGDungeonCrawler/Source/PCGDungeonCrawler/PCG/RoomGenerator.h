// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PCG/DungeonGenerationSubdivisionNode.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RoomGenerator.generated.h"

#define PCG_ROOM_GEN_TYPE_STR(RoomClassName) public: FString GetRoomTypeName(){return _roomTypeName;} private: static const inline FString _roomTypeName = TEXT(#RoomClassName);

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API ARoomGenerator : public AActor
{
	GENERATED_BODY()

	PCG_ROOM_GEN_TYPE_STR(ARoomGenerator)

public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void InitializeGenerator();
	virtual bool RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node);
	virtual void SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node);
};

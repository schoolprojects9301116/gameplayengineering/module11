// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/ClosableDoorObject.h"

AClosableDoorObject::AClosableDoorObject()
{
	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door Mesh"));
	DoorMesh->SetupAttachment(GetRootComponent());
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/GeneratableObject.h"

// Sets default values
AGeneratableObject::AGeneratableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//SceneLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Location"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	
	SetRootComponent(Mesh);
}

// Called when the game starts or when spawned
void AGeneratableObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGeneratableObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PCG/GeneratableObject.h"
#include "ClosableDoorObject.generated.h"

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API AClosableDoorObject : public AGeneratableObject
{
	GENERATED_BODY()

public:
	AClosableDoorObject();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OpenDoor();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void CloseDoor();

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* DoorMesh;
	
};

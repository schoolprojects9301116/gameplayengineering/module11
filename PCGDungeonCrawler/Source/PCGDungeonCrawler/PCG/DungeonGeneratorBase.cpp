// Fill out your copyright notice in the Description page of Project Settings.

#include "PCG/DungeonGeneratorBase.h"
#include "Math/UnrealMathUtility.h"
#include "PCG/DungeonGenerationSubdivisionNode.h"
#include "PCG/GeneratableObject.h"
#include "GameFramework/PlayerStart.h"
#include "PCG/RoomGenerator.h"

void UDungeonGeneratorBase::GenerateDungeon(UWorld* world, FDungeonGenerationInfo dungeonGenerationInfo
	, float x, float y)
{
	GenerateRoomGraph(dungeonGenerationInfo, x, y);
	GenerateLayout(world, dungeonGenerationInfo);
	for (auto& room : _roomNodes)
	{
		room->SpawnRoom(world, dungeonGenerationInfo.SpaceScaleConversion);
	}
}

void UDungeonGeneratorBase::GenerateRoomGraph(FDungeonGenerationInfo dungeonGenerationInfo
	, float x, float y)
{
	rootNode = UDungeonGenerationSubdivisionNode::CreateTree(dungeonGenerationInfo, x, y);
	GetAllRooms();
}

void UDungeonGeneratorBase::GetAllRooms()
{
	TArray<UDungeonGenerationSubdivisionNode*> tmpStack;
	tmpStack.Add(rootNode);
	while (!tmpStack.IsEmpty())
	{
		auto currentNode = tmpStack.Top();
		tmpStack.Pop();
		if (currentNode->IsLeafNode())
		{
			_roomNodes.Add(currentNode);
		}
		else
		{
			tmpStack.Push(currentNode->GetChildren()[0]);
			tmpStack.Push(currentNode->GetChildren()[1]);
		}
	}
}

void UDungeonGeneratorBase::GenerateLayout(UWorld* world, FDungeonGenerationInfo dungeonGenerationInfo)
{
	TArray<UDungeonGenerationSubdivisionNode*> changeableRooms = _roomNodes;
	auto startIdx = FMath::RandRange(0, changeableRooms.Num() - 1);
	_startRoom = changeableRooms[startIdx];
	changeableRooms.RemoveAt(startIdx);
	_endRoom = _startRoom;
	UDungeonGenerationSubdivisionNode* lastRoom = _endRoom;
	while (true)
	{
		auto connections = _endRoom->GetConnections();
		if (connections.Num() == 1 && _endRoom != _startRoom)
		{
			break;
		}
		auto rngIdx = FMath::RandRange(0, connections.Num() - 1);
		if (lastRoom == connections[rngIdx])
		{
			if (rngIdx == 0)
			{
				rngIdx++;
			}
			else
			{
				rngIdx--;
			}
		}
		lastRoom = _endRoom;
		_endRoom = connections[rngIdx];
	}

	auto endIdx = changeableRooms.IndexOfByKey(_endRoom);
	changeableRooms.RemoveAt(endIdx);

	_startGenerator = NewObject<ARoomGenerator>(ARoomGenerator::StaticClass(), dungeonGenerationInfo.StartRoomGenerator);
	_endGenerator = NewObject<ARoomGenerator>(ARoomGenerator::StaticClass(), dungeonGenerationInfo.EndRoomGenerator);
	if (_startGenerator)
	{
		_startGenerator->SetupRoom(world, _startRoom);
	}
	if (_endGenerator)
	{
		_endGenerator->SetupRoom(world, _endRoom);
	}

	//TMap<ARoomGenerator*, int> possibleGenerators;
	TArray<TPair<ARoomGenerator*, int>> possibleGenerators;
	for (auto& genType : dungeonGenerationInfo.PossibleRooms)
	{
		auto generator = world->SpawnActor<ARoomGenerator>(genType.Key);
		//auto generator = NewObject<ARoomGenerator>(ARoomGenerator::StaticClass(), genType.Key);
		if (generator)
		{
			generator->InitializeGenerator();
			possibleGenerators.Add(TPair<ARoomGenerator*, int>(generator, genType.Value));
		}
	}

	while (!changeableRooms.IsEmpty() && !possibleGenerators.IsEmpty())
	{
		auto generatorIdx = FMath::RandRange(0, possibleGenerators.Num() - 1);
		auto roomIdx = FMath::RandRange(0, changeableRooms.Num() - 1);
		auto & generatorPair = possibleGenerators[generatorIdx];
		auto room = changeableRooms[roomIdx];

		if (generatorPair.Key->RoomCanBeGenerated(room))
		{
			generatorPair.Key->SetupRoom(world, room);
			changeableRooms.RemoveAt(roomIdx);
			generatorPair.Value = generatorPair.Value - 1;
			if (generatorPair.Value == 0)
			{
				possibleGenerators.RemoveAt(generatorIdx);
			}
		}
		else
		{
			bool wasGenerated = false;
			for (roomIdx = 0; roomIdx < changeableRooms.Num(); roomIdx++)
			{
				room = changeableRooms[roomIdx];
				if (generatorPair.Key->RoomCanBeGenerated(room))
				{
					generatorPair.Key->SetupRoom(world, room);
					changeableRooms.RemoveAt(roomIdx);
					generatorPair.Value = generatorPair.Value - 1;
					if (generatorPair.Value == 0)
					{
						possibleGenerators.RemoveAt(generatorIdx);
					}
					wasGenerated = true;
					break;
				}
			}
			if (!wasGenerated)
			{
				possibleGenerators.RemoveAt(generatorIdx);
			}
		}
	}
}
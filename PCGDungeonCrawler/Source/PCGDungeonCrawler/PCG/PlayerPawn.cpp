// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/PlayerPawn.h"

#include "Components/CapsuleComponent.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	capsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	SetRootComponent(capsuleComponent);
	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	staticMesh->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddMovementInput(GetActorUpVector() * -1, GravityScale * DeltaTime);
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerPawn::MoveRight);
	PlayerInputComponent->BindAxis("CameraUp", this, &APlayerPawn::CameraUp);
	PlayerInputComponent->BindAxis("CameraRight", this, &APlayerPawn::CameraRight);
}

void APlayerPawn::MoveForward(float value)
{
	AddMovementInput(GetActorForwardVector(), value);
}

void APlayerPawn::MoveRight(float value)
{
	AddMovementInput(GetActorRightVector(), value);
}

void APlayerPawn::CameraUp(float value)
{
	//AddControllerPitchInput(value);
	AddControllerRollInput(value);
}

void APlayerPawn::CameraRight(float value)
{
	AddControllerYawInput(value);
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/DungeonPlacmentActor.h"

// Sets default values
ADungeonPlacmentActor::ADungeonPlacmentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADungeonPlacmentActor::BeginPlay()
{
	Super::BeginPlay();
	_generator = NewObject<UDungeonGeneratorBase>(UDungeonGeneratorBase::StaticClass(), _generatorType);
	if (_generator != nullptr)
	{
		_generator->GenerateDungeon(GetWorld(), GenerationInfo, GetActorLocation().X, GetActorLocation().Y);
	}
}

// Called every frame
void ADungeonPlacmentActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


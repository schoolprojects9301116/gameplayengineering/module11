// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/TriggerBoxGeneratable.h"
#include "PCG/PlayerPawn.h"
#include "Components/CapsuleComponent.h"

ATriggerBoxGeneratable::ATriggerBoxGeneratable()
{
	_triggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	_triggerBox->SetCollisionProfileName(TEXT("Trigger"));
	_triggerBox->SetupAttachment(GetRootComponent());
}

void ATriggerBoxGeneratable::BeginPlay()
{
	_triggerBox->OnComponentBeginOverlap.AddUniqueDynamic(this, &ATriggerBoxGeneratable::OnOverlapBegin);
}

ATriggerBoxGeneratable::FTriggerTileEvent& ATriggerBoxGeneratable::OnTileTriggered()
{
	return tileTriggeredEvent;
}

void ATriggerBoxGeneratable::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
	, const FHitResult& SweepResult)
{
	if (Cast<APlayerPawn>(OtherActor) && Cast<UCapsuleComponent>(OtherComp))
	{
		tileTriggeredEvent.Broadcast();
	}
}
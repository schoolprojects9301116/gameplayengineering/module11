// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DungeonGeneratorBase.generated.h"

class AGeneratableObject;
class ARoomGenerator;

USTRUCT()
struct PCGDUNGEONCRAWLER_API FDungeonGenerationInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	int SpaceWidth = 5;

	UPROPERTY(EditAnywhere)
	int SpaceHeight = 5;

	UPROPERTY(EditAnywhere)
	int MinRoomWidth = 1;

	UPROPERTY(EditAnywhere)
	int MaxRoomWidth = 2;

	UPROPERTY(EditAnywhere)
	int MinRoomLength = 1;

	UPROPERTY(EditAnywhere)
	int MaxRoomLength = 2;

	UPROPERTY(EditAnywhere)
	int MinRoomSpacing = 1;
	
	UPROPERTY(EditAnywhere)
	int MinRoomCount = 2;

	UPROPERTY(EditAnywhere)
	float SpaceScaleConversion = 256;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AGeneratableObject> wallType;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AGeneratableObject> floorType;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AGeneratableObject> doorType;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ARoomGenerator> StartRoomGenerator;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ARoomGenerator> EndRoomGenerator;

	UPROPERTY(EditAnywhere)
	TMap<TSubclassOf<ARoomGenerator>, int> PossibleRooms;
};


class UDungeonGenerationSubdivisionNode;
class UWorld;

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API UDungeonGeneratorBase : public UObject
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	void GenerateDungeon(UWorld* world, FDungeonGenerationInfo dungeonGenerationInfo, float x, float y);

protected:

	UFUNCTION()
	void GenerateRoomGraph(FDungeonGenerationInfo dungeonGenerationInfo, float x, float y);

	UFUNCTION()
	void GenerateLayout(UWorld* world, FDungeonGenerationInfo dungeonGenerationInfo);

	UFUNCTION()
	void GetAllRooms();

	UPROPERTY()
	TArray<UDungeonGenerationSubdivisionNode*> _roomNodes;

	UPROPERTY()
	UDungeonGenerationSubdivisionNode* rootNode = nullptr;

	UPROPERTY()
	UDungeonGenerationSubdivisionNode* _startRoom;

	UPROPERTY()
	UDungeonGenerationSubdivisionNode* _endRoom;

	UPROPERTY()
	ARoomGenerator* _startGenerator;

	UPROPERTY()
	ARoomGenerator* _endGenerator;
};

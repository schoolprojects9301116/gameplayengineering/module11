// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PCG/DungeonGeneratorBase.h"
#include "PCG/GeneratableObject.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DungeonGenerationSubdivisionNode.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class PCGDUNGEONCRAWLER_API UDungeonGenerationSubdivisionNode : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
	TArray<AGeneratableObject*> SpawnedTiles;

	UFUNCTION()
	static UDungeonGenerationSubdivisionNode* CreateTree(
			FDungeonGenerationInfo dungeonGenerationInfo
		, int x, int y);

	UFUNCTION()
	const TArray<UDungeonGenerationSubdivisionNode*>& GetChildren() const;

	UFUNCTION()
	const TArray<UDungeonGenerationSubdivisionNode*>& GetConnections() const;

	UFUNCTION()
	bool IsLeafNode() const;

	UFUNCTION()
	int X() const;

	UFUNCTION()
	int Y() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int Width() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int Length() const;

	UFUNCTION()
	bool ConnectToNode(UDungeonGenerationSubdivisionNode* otherNode, TSubclassOf<AGeneratableObject> doorType);

	UFUNCTION(BlueprintCallable)
	void SetTile(int x, int y, TSubclassOf<AGeneratableObject> tileType);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<AGeneratableObject> GetTile(int x, int y) const;

	DECLARE_EVENT(UDungeonGenerationSubdivisionNode, FRoomSpawnEvent)
	FRoomSpawnEvent& OnRoomSpawnCompleted();

	UFUNCTION()
	void SpawnRoom(UWorld * world, float tileScale);

	UPROPERTY()
	FString CurrentRoomType;

protected:

	UFUNCTION()
	void SetupWalls(TSubclassOf<AGeneratableObject> wallType);

	int InitializeNode(FDungeonGenerationInfo dungeonGenerationInfo
		, int x, int y, int width, int length);
	int _x;
	int _y;
	int _width;
	int _length;

	UPROPERTY()
	TArray<UDungeonGenerationSubdivisionNode*> _children;

	UPROPERTY()
	TArray<UDungeonGenerationSubdivisionNode*> _connectedRooms;

	UPROPERTY()
	TArray<TSubclassOf<AGeneratableObject>> _tileMap;

	TSubclassOf<AGeneratableObject> defaultTile;

	FRoomSpawnEvent _roomSpawnEvent;
};

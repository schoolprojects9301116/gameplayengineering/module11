// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PCG/RoomTypes/PuzzleRoomGenerator.h"
#include "MusicPuzzleRoomGenerator.generated.h"

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API AMusicPuzzleRoomGenerator : public APuzzleRoomGenerator
{
	GENERATED_BODY()
	
	PCG_ROOM_GEN_TYPE_STR(APuzzleRoomGenerator)

public:
	virtual bool RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node) override;
	virtual void SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node) override;

protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ATriggerBoxGeneratable> _puzzleStartBlock;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ATriggerBoxGeneratable> _puzzleBlocks;
};

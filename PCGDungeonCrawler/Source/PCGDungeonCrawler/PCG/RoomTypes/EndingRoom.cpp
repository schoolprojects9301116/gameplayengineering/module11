// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/RoomTypes/EndingRoom.h"

bool AEndingRoom::RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node)
{
	return true;
}

void AEndingRoom::SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node)
{
	node->CurrentRoomType = _roomTypeName;
	node->SetTile(node->Width() / 2, node->Length() / 2, _endType);
}
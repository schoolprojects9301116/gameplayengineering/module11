// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PCG/RoomGenerator.h"
#include "EndingRoom.generated.h"

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API AEndingRoom : public ARoomGenerator
{
	GENERATED_BODY()
	
	PCG_ROOM_GEN_TYPE_STR(AEndingRoom)

public:
	virtual bool RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node) override;
	virtual void SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node) override;

protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<AGeneratableObject> _endType;
};

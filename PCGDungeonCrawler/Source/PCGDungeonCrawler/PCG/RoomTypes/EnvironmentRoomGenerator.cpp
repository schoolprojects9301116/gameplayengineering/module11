// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/RoomTypes/EnvironmentRoomGenerator.h"

bool AEnvironmentRoomGenerator::RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node)
{
	return true;
}

void AEnvironmentRoomGenerator::SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node)
{
	// Ensure we have tiles to place
	if (_tileTypes.Num() == 0)
	{
		return;
	}

	// Get Keys for random selection
	TArray<TSubclassOf<AGeneratableObject>> tileTypes;
	_tileTypes.GenerateKeyArray(tileTypes);

	// Get Number of spawnable types for probability calculation
	int numSpawnableTiles = 0;
	for (int x = 2; x < node->Width() - 2; x++)
	{
		for (int y = 2; y < node->Length() - 2; y++)
		{
			if (!node->GetTile(x, y))
			{
				numSpawnableTiles++;
			}
		}
	}

	int numTilesToSpawn = FMath::RandRange(_minTilesToSpawn, _maxTilesToSpawn);

	// Calculate tile chance
	float chancePerTile = 1.f / ((float)numSpawnableTiles - (float)numTilesToSpawn);
	float rng = FMath::RandRange(0.f, 1.f);

	for (int x = 2; x < node->Width() - 2; x++)
	{
		for (int y = 2; y < node->Length() - 2; y++)
		{
			if (numTilesToSpawn == 0)
			{
				return;
			}
			// IF it is a spawnable tile
			if (!node->GetTile(x, y))
			{
				numSpawnableTiles--;
				//Spawn tile and recalculate probability
				if (rng < chancePerTile)
				{
					auto roomType = tileTypes[FMath::RandRange(0, tileTypes.Num() - 1)];
					auto & rule = _tileTypes[tileTypes[FMath::RandRange(0, tileTypes.Num() - 1)]];
	
					// Spawn using the rule if it exists, otherwise just spawn the tiletype
					GEngine->AddOnScreenDebugMessage(1, 3.f, FColor::Yellow, rule.GetFunctionName().ToString());
					if (rule.IsBound())
					{
						rule.Execute(node, x, y);
					}
					else
					{
						node->SetTile(x, y, roomType);
					}

					// Re-Calculate Chance
					numTilesToSpawn--;
					chancePerTile = 1.f / ((float)numSpawnableTiles - (float)numTilesToSpawn);
					rng = FMath::RandRange(0.f, 1.f);
				}
				else
				{
					rng -= chancePerTile;
				}
			}
		}
	}
}

void AEnvironmentRoomGenerator::SetTileSpawn(TSubclassOf<AGeneratableObject> tileType, FEnvironmentSpawnDelegate spawnDelegate)
{
	_tileTypes[tileType] = spawnDelegate;
}

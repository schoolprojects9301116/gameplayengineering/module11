// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/RoomTypes/PuzzleRoomGenerator.h"
#include "PCG/ClosableDoorObject.h"
#include "PCG/TriggerBoxGeneratable.h"

#include "PCG/PuzzleRoomManager.h"

bool APuzzleRoomGenerator::RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node)
{
	return(node->GetConnections().Num() > 1);
}

void APuzzleRoomGenerator::SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node)
{
	if (world != nullptr)
	{
		puzzleManager = world->SpawnActor<APuzzleRoomManager>(_puzzleManager);
		if (puzzleManager)
		{
			puzzleManager->SetupRoom(node);
		}
	}
	for (int x = 0; x < node->Width(); x++)
	{
		TSubclassOf<AClosableDoorObject> doorCheck;
		doorCheck = node->GetTile(x, 0);//->GetClass();
		if (doorCheck)
		{
			GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Green, TEXT("DOOR FOUND"));
			node->SetTile(x, 1, _triggerBoxType);
		}
		doorCheck = node->GetTile(x, node->Length() - 1);//->GetClass();
		if (doorCheck)
		{
			node->SetTile(x, node->Length() - 2, _triggerBoxType);
			GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Green, TEXT("DOOR FOUND"));
		}
	}
	for (int y = 0; y < node->Length(); y++)
	{
		TSubclassOf<AClosableDoorObject> doorCheck;
		doorCheck = node->GetTile(0, y);//->GetClass();
		if (doorCheck)
		{
			node->SetTile(1, y, _triggerBoxType);
			GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Green, TEXT("DOOR FOUND"));
		}
		doorCheck = node->GetTile(node->Width() - 1, y);//->GetClass();
		if (doorCheck)
		{
			node->SetTile(node->Width() - 2, y, _triggerBoxType);
			GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Green, TEXT("DOOR FOUND"));
		}
	}
}
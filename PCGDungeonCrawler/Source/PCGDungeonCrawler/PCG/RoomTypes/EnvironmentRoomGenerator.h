// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PCG/RoomGenerator.h"
#include "EnvironmentRoomGenerator.generated.h"

UDELEGATE()
DECLARE_DYNAMIC_DELEGATE_ThreeParams(FEnvironmentSpawnDelegate, UDungeonGenerationSubdivisionNode*, node, int, x, int, y);

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API AEnvironmentRoomGenerator : public ARoomGenerator
{
	GENERATED_BODY()

	PCG_ROOM_GEN_TYPE_STR(AEnvironmentRoomGenerator)

public:
	virtual bool RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node) override;
	virtual void SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node) override;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<TSubclassOf<AGeneratableObject>, FEnvironmentSpawnDelegate> _tileTypes;

	UPROPERTY(EditAnywhere)
	int _minTilesToSpawn;

	UPROPERTY(EditAnywhere)
	int _maxTilesToSpawn;

	UFUNCTION(BlueprintCallable)
	void SetTileSpawn(TSubclassOf<AGeneratableObject> tileType, FEnvironmentSpawnDelegate spawnDelegate);
};

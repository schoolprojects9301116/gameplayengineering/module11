// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PCG/TriggerBoxGeneratable.h"

#include "CoreMinimal.h"
#include "PCG/RoomGenerator.h"
#include "PuzzleRoomGenerator.generated.h"

class AClosableDoorObject;
class APuzzleRoomManager;

/**
 * 
 */
UCLASS()
class PCGDUNGEONCRAWLER_API APuzzleRoomGenerator : public ARoomGenerator
{
	GENERATED_BODY()

	PCG_ROOM_GEN_TYPE_STR(APuzzleRoomGenerator)

public:
	virtual bool RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node) override;
	virtual void SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node) override;

protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ATriggerBoxGeneratable> _triggerBoxType;

	UPROPERTY(EditAnywhere)
	TSubclassOf<APuzzleRoomManager> _puzzleManager;

	UPROPERTY()
	APuzzleRoomManager* puzzleManager = nullptr;
};

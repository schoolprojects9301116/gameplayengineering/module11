// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/RoomTypes/MusicPuzzleRoomGenerator.h"
#include "PCG/MusicPuzzleManager.h"

bool AMusicPuzzleRoomGenerator::RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node)
{
	bool roomCanBeGenerated = (node->Width() >= 9 && node->Length() >= 7) || (node->Length() >= 9 && node->Width() >= 7);
	return Super::RoomCanBeGenerated(node) && roomCanBeGenerated;
}

void AMusicPuzzleRoomGenerator::SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node)
{
	Super::SetupRoom(world, node);
	auto musicMgr = Cast<AMusicPuzzleManager>(puzzleManager);
	if (musicMgr)
	{
		musicMgr->SetBlockTypes(_puzzleStartBlock, _puzzleBlocks);
	}

	int w = FMath::RoundToInt((float)node->Width() / 2.f);
	int l = FMath::RoundToInt((float)node->Length() / 2.f);
	bool widerThanLong = w > l;
	int& centerPos = widerThanLong ? w : l;
	int& middlePos = widerThanLong ? l : w;

	TSubclassOf<AGeneratableObject> tileType;
	tileType = _puzzleStartBlock;
	middlePos -= 1;
	node->SetTile(w, l, tileType);
	middlePos += 2;
	centerPos -= 4;
	tileType = _puzzleBlocks;
	for (int i = 0; i < 3; i ++)
	{
		centerPos += 2;
		node->SetTile(w, l, tileType);
	}
}
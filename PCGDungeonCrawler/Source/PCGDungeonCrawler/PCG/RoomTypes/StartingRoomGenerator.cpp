// Fill out your copyright notice in the Description page of Project Settings.


#include "PCG/RoomTypes/StartingRoomGenerator.h"


bool AStartingRoomGenerator::RoomCanBeGenerated(UDungeonGenerationSubdivisionNode* node)
{
	return true;
}

void AStartingRoomGenerator::SetupRoom(UWorld* world, UDungeonGenerationSubdivisionNode* node)
{
	node->CurrentRoomType = _roomTypeName;

	FIntPoint centerPoint(node->Width() / 2, node->Length() / 2);
	node->SetTile(centerPoint.X, centerPoint.Y, startingType);
	node->SetTile(centerPoint.X + 1, centerPoint.Y + 1, columnType);
	node->SetTile(centerPoint.X + 1, centerPoint.Y - 1, columnType);
	node->SetTile(centerPoint.X - 1, centerPoint.Y + 1, columnType);
	node->SetTile(centerPoint.X - 1, centerPoint.Y - 1, columnType);
}
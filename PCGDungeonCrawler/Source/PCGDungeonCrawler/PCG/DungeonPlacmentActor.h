// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PCG/DungeonGeneratorBase.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DungeonPlacmentActor.generated.h"

UCLASS()
class PCGDUNGEONCRAWLER_API ADungeonPlacmentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADungeonPlacmentActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	FDungeonGenerationInfo GenerationInfo;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UDungeonGeneratorBase> _generatorType;

	UPROPERTY()
	UDungeonGeneratorBase* _generator;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

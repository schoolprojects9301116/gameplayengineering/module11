// Copyright Epic Games, Inc. All Rights Reserved.

#include "PCGDungeonCrawler.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PCGDungeonCrawler, "PCGDungeonCrawler" );
